import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tp4.model.InstanceTSP;
import tp4.solveur.SolveurAjoutPlusProche;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class SolveurAjoutDuPlusProcheTest {

    private SolveurAjoutPlusProche solveurAjoutPlusProche;

    @BeforeEach
    void setup() {
        Map<Integer, Map<Integer, Integer>> data = new HashMap<>();
        Map<Integer, Integer> line1 = new HashMap<>();
        line1.put(0, 99);
        line1.put(1, 2);
        line1.put(2, 3);
        Map<Integer, Integer> line2 = new HashMap<>();
        line2.put(0, 20);
        line2.put(1, 99);
        line2.put(2, 10);
        Map<Integer, Integer> line3 = new HashMap<>();
        line3.put(0, 20);
        line3.put(1, 10);
        line3.put(2, 99);
        data.put(0, line1);
        data.put(1, line2);
        data.put(2, line3);

        InstanceTSP instanceTSP = new InstanceTSP("test data", 3, data, "nofile");
        solveurAjoutPlusProche = new SolveurAjoutPlusProche(instanceTSP);
        solveurAjoutPlusProche.setStartCity(0);
    }

    @Test
    void should_get_closest_minimum() {
        // arrange
        Map<Integer, Integer> line = new HashMap<>();
        line.put(0, 999);
        line.put(1, 5);
        line.put(2, 8);

        // act
        Integer closestMinimum = solveurAjoutPlusProche.getClosestMinimum(line);

        // assert
        assertThat(closestMinimum).isEqualTo(1);
    }

    @Test
    void should_move_to_closest_minimum() {
        // arrange
        Map<Integer, Integer> line = solveurAjoutPlusProche.getInstance()
                                                           .getData().get(0);
        Integer closestMinimum = solveurAjoutPlusProche.getClosestMinimum(line);

        // act
        assertThat(closestMinimum).isEqualTo(1);
    }

    @Test
    void should_return_a_tsp_solution_of_size_dimention_plus_one() {
        // arrange
        int dimension = solveurAjoutPlusProche.getInstance().getDimension();

        // act
        List<Integer> result = solveurAjoutPlusProche.buildFullPath();

        // assert
        assertThat(result).hasSize(dimension + 1);
    }

    @Test
    void should_return_expected_path() {
        // act
        List<Integer> path = solveurAjoutPlusProche.buildFullPath();

        // assert
        assertThat(path).containsExactly(0, 1, 2, 0);
    }

    @Test
    void should_calculate_distance() {
        // arrange
        solveurAjoutPlusProche.buildFullPath();

        // act
        Integer distance = solveurAjoutPlusProche.getDistance();

        // assert
        assertThat(distance).isEqualTo(32);
    }

}
