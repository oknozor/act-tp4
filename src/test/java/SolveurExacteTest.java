import org.junit.jupiter.api.Test;
import tp4.model.Arc;
import tp4.model.Node;

import static org.assertj.core.api.Assertions.assertThat;

public class SolveurExacteTest {
    @Test
    public void should_get_revesed_arc() {
        // arrange
        Arc arc = new Arc(0, 1, 10);
        Arc reverse = new Arc(1, 0, 20);
        Arc other = new Arc(1, 0, 20);
        Node node = new Node();
        node.add(arc);
        node.add(reverse);
        node.add(other);

        // act
        Arc reversed = node.getReverse(arc);

        // assert
        assertThat(reversed.getFrom()).isEqualTo(reverse.getFrom());
        assertThat(reversed.getTo()).isEqualTo(reverse.getTo());
        assertThat(reversed.getDistance()).isEqualTo(reverse.getDistance());
    }
}
