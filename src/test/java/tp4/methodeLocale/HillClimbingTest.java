package tp4.methodeLocale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tp4.model.InstanceTSP;
import tp4.solveur.SolveurAjoutPlusProche;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class HillClimbingTest {

    HillClimbing hillClimbing;

    @BeforeEach
    void setup() {
        Map<Integer, Map<Integer, Integer>> data = new HashMap<>();
        Map<Integer, Integer> line1 = new HashMap<>();
        line1.put(0, 999);
        line1.put(1, 5);
        line1.put(2, 10);
        line1.put(3, 15);
        line1.put(4, 20);
        Map<Integer, Integer> line2 = new HashMap<>();
        line2.put(0, 25);
        line2.put(1, 999);
        line2.put(2, 30);
        line2.put(3, 35);
        line2.put(4, 40);
        Map<Integer, Integer> line3 = new HashMap<>();
        line3.put(0, 45);
        line3.put(1, 50);
        line3.put(2, 999);
        line3.put(3, 55);
        line3.put(4, 60);
        Map<Integer, Integer> line4 = new HashMap<>();
        line4.put(0, 65);
        line4.put(1, 70);
        line4.put(2, 75);
        line4.put(3, 999);
        line4.put(4, 80);
        Map<Integer, Integer> line5 = new HashMap<>();
        line5.put(0, 85);
        line5.put(1, 90);
        line5.put(2, 95);
        line5.put(3, 100);
        line5.put(4, 999);
        data.put(0, line1);
        data.put(1, line2);
        data.put(2, line3);
        data.put(3, line4);
        data.put(4, line5);

        InstanceTSP instanceTSP = new InstanceTSP("test data", 5, data, "nofile");
        hillClimbing = new HillClimbing(instanceTSP, new SolveurAjoutPlusProche(instanceTSP));
    }

    @Test
    public void findNextNeighbour() {
    }

    @Test
    public void swapCities() {
        List<Integer> citiesNeighbour = new ArrayList<Integer>();
        citiesNeighbour.add(1);
        citiesNeighbour.add(2);
        citiesNeighbour.add(3);
        citiesNeighbour.add(4);
        citiesNeighbour.add(5);

        List<Integer> citiesNeigbourSwaped = new ArrayList<Integer>();

    }

    @Test
    public void initializeNeighbour() {
        List<Integer>citiesNeighbour = new ArrayList<Integer>();
        citiesNeighbour.add(1);
        citiesNeighbour.add(2);
        citiesNeighbour.add(3);
        citiesNeighbour.add(4);
        citiesNeighbour.add(5);

        List<Integer> citiesNeighbourInitialize = new ArrayList<>();
        hillClimbing.initializeNeighbour(citiesNeighbour, citiesNeighbourInitialize, 3);


        assertThat(citiesNeighbourInitialize.get(0)).isEqualTo(citiesNeighbour.get(0));
        assertThat(citiesNeighbourInitialize.get(1)).isEqualTo(citiesNeighbour.get(1));
        assertThat(citiesNeighbourInitialize.get(2)).isEqualTo(citiesNeighbour.get(2));

        assertThatThrownBy(() -> citiesNeighbourInitialize.get(3)).isInstanceOf(IndexOutOfBoundsException.class);

    }

    @Test
    public void findDistance() {
        List<Integer>citiesNeighbour = new ArrayList<Integer>();
        citiesNeighbour.add(0);
        citiesNeighbour.add(3);
        citiesNeighbour.add(2);
        citiesNeighbour.add(4);
        citiesNeighbour.add(1);

        assertThat(hillClimbing.findDistance(citiesNeighbour)).isEqualTo(240);
    }
}