package tp4.exception;

public class NoDataFoundException extends RuntimeException {
    public NoDataFoundException(String fileName) {
        super(String.format("Data not found for file %s", fileName));
    }
}
