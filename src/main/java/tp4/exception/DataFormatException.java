package tp4.exception;

public class DataFormatException extends RuntimeException{
    public DataFormatException(String filename) {
        super(String.format("Unknown error occured reading data from %s", filename));
    }
}
