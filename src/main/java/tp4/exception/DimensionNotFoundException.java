package tp4.exception;

public class DimensionNotFoundException extends RuntimeException {
    public DimensionNotFoundException(String filename) {
        super(String.format("Dimension not found for file %s", filename));
    }
}
