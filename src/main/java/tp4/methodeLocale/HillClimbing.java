package tp4.methodeLocale;

import tp4.model.InstanceTSP;
import tp4.solveur.AbstractSolveur;
import tp4.solveur.SolveurAjoutPlusProche;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class HillClimbing extends AbstractSolveur {

    private SolveurAjoutPlusProche solveurAjoutPlusProche;

    public HillClimbing(InstanceTSP instance, SolveurAjoutPlusProche solveurAjoutPlusProche) {
        super(instance);
        this.solveurAjoutPlusProche = solveurAjoutPlusProche;
    }

    public List<Integer> algorithm() {

        List<Integer> cities = new ArrayList<Integer>(this.solveurAjoutPlusProche.buildFullPath());
        List<Integer> neighbour = new ArrayList<Integer>(Collections.nCopies(cities.size(), 0));
        List<Integer> bestNeighbour = new ArrayList<Integer>(cities);

        int bestDistance = solveurAjoutPlusProche.getDistance();
        int distanceNeighbour;


        for (int j = 0; j < cities.size() -1 ; j ++) {
            for (int i = 1; i < j - 1; i++) {
                neighbour = findNextNeighbour(cities, j, i);
                distanceNeighbour = findDistance(neighbour);

                if (distanceNeighbour < bestDistance) {
                    bestDistance = distanceNeighbour;
                    Collections.copy(bestNeighbour, neighbour);
                }
            }
        }
        this.distance = bestDistance;
        return bestNeighbour;
    }

    public List<Integer> findNextNeighbour(List<Integer> cities, int j, int i) {

        List<Integer>citiesNeighbour = new ArrayList<Integer>(Collections.nCopies(cities.size(), 0));
        initializeNeighbour(cities, citiesNeighbour, i);
        citiesNeighbour.set(i+1, cities.get(j));
        swapCities(cities, citiesNeighbour, i, j);
        addRemainingCities(cities, citiesNeighbour, j);
        return citiesNeighbour;
    }

    public void swapCities(List<Integer> cities, List<Integer> citiesNeighbour, int i, int j) {
        for (int p = 1; p < j - i + 1; p++) {
            citiesNeighbour.set((j + 1 - p),cities.get(i + p));
        }
    }

    public void initializeNeighbour(List<Integer> data, List<Integer> citiesNeighbour, int i) {
        for (int k = 0; k < i + 1; k++) {
            citiesNeighbour.set(k, data.get(k));
        }
    }

    public void addRemainingCities(List<Integer> data, List<Integer> citiesNeighbour, int j) {
        for (int k = j + 1; k < data.size(); k ++) {
            citiesNeighbour.set(k, data.get(k));
        }
    }


    public int findDistance(List<Integer> cityList) {
        int size = cityList.size() - 1;
        int distanceToReturn = 0;
        for (int i = 0; i < size; i++) {
            int currentCity = cityList.get(i);
            int nextCity = cityList.get(i + 1);
            distanceToReturn += instance.getData().get(currentCity).get(nextCity);
        }
        return distanceToReturn;
    }
}
