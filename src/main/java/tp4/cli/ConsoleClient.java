package tp4.cli;

import tp4.methodeLocale.HillClimbing;
import tp4.model.InstanceTSP;
import tp4.solveur.SolveurAjoutPlusProche;
import tp4.solveur.SolveurExacte;
import tp4.util.DataLoader;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class ConsoleClient {
    private static final String red = "\033[31m";
    private static final String green = "\033[32m";
    private static final String yellow = "\033[33m";
    private static final String blue = "\033[34m";
    private static final String magenta = "\033[35m";
    private static final String cyan = "\033[36m";
    private static final String grey = "\033[37m";
    private static final String reset = "\033[0m";

    private List<InstanceTSP> instances;
    private Scanner sc;
    private List<File> files;

    public ConsoleClient() {
        this.instances = new ArrayList<>();
        this.sc = new Scanner(System.in);
        File directory = new File("src/main/resources/");
        this.files = Arrays.asList(Objects.requireNonNull(directory.listFiles()));
    }

    private void loadFile() {
        DataLoader dataLoader = new DataLoader();

        files.forEach(f -> {
            InstanceTSP instance = (dataLoader.buildInstance(f.getPath()));
            this.instances.add(instance);
        });
    }

    public void commandLoop() {
        loadFile();
        welcome();
        help();
        resetPrompt();
        while (sc.hasNextLine()) {
            String input = sc.nextLine();
            List<String> commandAndArgs = Arrays.asList(input.split("\\s+"));
            Command command = Command.typeOf(commandAndArgs.get(0));
            if (commandAndArgs.size() == 1) {
                command.execute(this);
            } else if (commandAndArgs.size() == 2) {
                try {
                    Integer argAsInt = Integer.parseInt(commandAndArgs.get(1));
                    command.executeWithArgs(this, argAsInt);
                } catch (NumberFormatException e) {
                    System.err.print("Enter a valid instance number please!");
                } catch (IndexOutOfBoundsException e) {
                    System.err.println("Instance does not exist");
                    System.err.print("Enter an instance number beetween 0 and " + (instances.size() - 1));
                }
            } else {
                System.err.print(red + "Too many argument");
            }
            resetPrompt();
        }
    }

    public void solveWithClosestMethod(Integer instanceNumber) {
        InstanceTSP instanceTSP = instances.get(instanceNumber);
        SolveurAjoutPlusProche solveurAjoutPlusProche = new SolveurAjoutPlusProche(instanceTSP);
        System.out.println("Pick a starting city from 0 to " + instanceTSP.getDimension() + " : ");
        resetPrompt();
        Scanner localSc = new Scanner(System.in);
        Integer startCity = localSc.nextInt();
        solveurAjoutPlusProche.setStartCity(startCity);

        System.out.println(instanceTSP.getComment() + ":");
        System.out.print(magenta + "Path : " + reset);

        List<Integer> fullPath = solveurAjoutPlusProche.buildFullPath();
        int lastElement = fullPath.size() - 1;
        IntStream.range(0, lastElement)
                .forEach(i -> System.out.print(fullPath.get(i) + " -> "));
        System.out.print(fullPath.get(fullPath.size()-1));

        System.out.println();
        System.out.println(magenta + "Distance : " + solveurAjoutPlusProche.getDistance());
    }

    public void solveWithExactMethod(Integer instanceNumber) {
        InstanceTSP instanceTSP = instances.get(instanceNumber);
        SolveurExacte solveurExacte = new SolveurExacte(instanceTSP);
        solveurExacte.solve();
    }

    private void welcome() {
        System.out.println(" ====================================================================================");
        System.out.println(" = Welcome to" + cyan + "Thibaut Strecker" + reset + "And" + cyan + "Paul Delafosse" + reset + "Traveling Salesman Problem solver =");
        System.out.println(" ====================================================================================");
    }

    public void listFile() {
        AtomicInteger i = new AtomicInteger();

        System.out.println();
        System.out.println(yellow + "N°\tFILE\t\tDESCRIPTION");
        System.out.println(grey);

        instances.forEach(f -> {
            System.out.format(blue + "%s" + "\t" + green + "%s" + "\t" + grey + "%s",
                              i.getAndIncrement(), f.getFilename(), f.getComment());
            System.out.println();

        });

    }

    public void solveWithHillClimbing(Integer instanceNumber) {
        InstanceTSP instanceTSP = instances.get(instanceNumber);
        SolveurAjoutPlusProche solveurAjoutPlusProche = new SolveurAjoutPlusProche(instanceTSP);
        HillClimbing hillClimbing = new HillClimbing(instanceTSP, solveurAjoutPlusProche);
        System.out.println("Pick a starting city from 0 to " + instanceTSP.getDimension() + " : ");
        resetPrompt();
        Scanner localSc = new Scanner(System.in);
        Integer startCity = localSc.nextInt();
        solveurAjoutPlusProche.setStartCity(startCity);
        hillClimbing.setStartCity(startCity);
        List<Integer> fullPath = hillClimbing.algorithm();

        System.out.println(instanceTSP.getComment() + ":");
        System.out.print(magenta + "Path : " + reset);

        int lastElement = fullPath.size() - 2;
        fullPath.stream()
                .filter(integer -> !integer.equals(lastElement))
                .forEach(integer -> System.out.print(integer + " -> "));
        System.out.println();
        System.out.println(magenta + "Distance : " + hillClimbing.getDistance());
    }

    public void print(Integer instanceNumber) {
        InstanceTSP currentInstance = instances.get(instanceNumber);
        System.out.println(currentInstance.getComment());

        int maxCharacter = currentInstance.getData().get(0).get(0).toString().length();

        System.out.print("\t");
        currentInstance.getData().forEach((key, value) -> buildCell(maxCharacter, key, true));
        System.out.print(magenta+  "|" + reset);
        System.out.println(reset);

        currentInstance.getData().forEach((key, value) -> {
            System.out.print(green + key + reset);
            System.out.print("\t");
            currentInstance.getData().get(key).values().forEach(integer -> {
                buildCell(maxCharacter, integer, false);
            });
            System.out.println(magenta + "|" + reset);
        });
    }

    private void buildCell(int maxCharacter, Integer integer, boolean color) {
        int whiteSpaceRange = maxCharacter - integer.toString().length();
        int sideWhiteSpaceRange = whiteSpaceRange / 2;
        int rightWhiteSpace = whiteSpaceRange % 2 == 0 ? sideWhiteSpaceRange : sideWhiteSpaceRange + 1;

        if (integer.toString().length() == maxCharacter) {
            rightWhiteSpace = 0;
        }

        StringBuilder cellBuilder = new StringBuilder();
        cellBuilder.append(magenta + "|" + reset);
        if (color) {
            cellBuilder.append(green);
        }
        IntStream.range(0, sideWhiteSpaceRange).forEach(i -> cellBuilder.append(" "));

        cellBuilder.append(integer);
        System.out.print(reset);
        IntStream.range(0, rightWhiteSpace).forEach(i -> cellBuilder.append(" "));
        System.out.print(cellBuilder.toString());
    }

    public void help() {
        System.out.println();
        System.out.println("Commands:");
        Arrays.stream(Command.values())
              .filter(command -> !command.equals(Command.OTHER))
              .forEach(command -> {
                  System.out.print(cyan + command.printName());
                  System.out.print(reset + "\t" + command.hint());
                  System.out.println();
              });
    }

    public void exit() {
        System.exit(0);
    }

    public void unknownCommand() {
        System.err.print(Command.OTHER.hint());
    }

    private void resetPrompt() {
        System.out.println();
        System.out.print(reset+">");
    }

    public void noArgError() {
        System.err.println("Please supply an instance number!");

    }
}

