package tp4.cli;

import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public enum Command {
    LIST_FILE(
            "list",
            "\t\tDisplay the list of avalaible isntances",
            ConsoleClient::listFile,
            (consoleClient, integer) -> consoleClient.listFile()
    ),
    PRINT_INSTANCE(
            "print",
            "[OPT] \tPrint current selected instance data and description",
            ConsoleClient::noArgError,
            ConsoleClient::print
    ),
    SOLVE_ADD(
            "closest",
            "[OPT] \tSolve selected with closest add method instance and prompt result path and distance",
            ConsoleClient::noArgError,
            ConsoleClient::solveWithClosestMethod
    ),
    SOLVE_EXACT(
            "exact",
            "[OPT] \tSolve selected with exact method instance and prompt result path and distance",
            ConsoleClient::noArgError,
            ConsoleClient::solveWithExactMethod

    ),
    HILLCLIMBING(
            "solvehc",
            "[OPT] \tSolve selected instance and prompt result path and distance using Hill Climbing method",
            ConsoleClient::noArgError,
            ConsoleClient::solveWithHillClimbing
    ),
    HELP(
            "help",
            "\t\tDisplay help",
            ConsoleClient::help,
            (consoleClient, integer) -> consoleClient.help()
    ),
    EXIT(
            "exit",
            "\t\tExit the command line interface",
            ConsoleClient::exit,
            (consoleClient, integer) -> consoleClient.exit()
    ),
    OTHER(
            "other",
            "Unkown command",
            ConsoleClient::unknownCommand,
            (consoleClient, integer) -> consoleClient.unknownCommand()
    );

    private String name;
    private String hint;
    private Consumer<ConsoleClient> clientConsumer;
    private BiConsumer<ConsoleClient, Integer> clientWithArgConsumer;

    Command(String name, String hint, Consumer<ConsoleClient> clientConsumer, BiConsumer<ConsoleClient, Integer> clientWithArgConsumer) {
        this.hint = hint;
        this.name = name;
        this.clientWithArgConsumer = clientWithArgConsumer;
        this.clientConsumer = clientConsumer;
    }

    static Command typeOf(String command) {
        return Arrays.stream(Command.values())
                     .filter(type -> command.toLowerCase().equals(type.name))
                     .findFirst().orElse(OTHER);
    }

    public void execute(ConsoleClient consoleClient) {
        clientConsumer.accept(consoleClient);
    }

    public void executeWithArgs(ConsoleClient consoleClient, Integer instanceNumber) {
        clientWithArgConsumer.accept(consoleClient, instanceNumber);
    }

    public String hint() {
        return hint;
    }

    public String printName() {
        return name;
    }
}
