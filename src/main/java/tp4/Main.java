package tp4;

import tp4.cli.ConsoleClient;

public class Main {

    public static void main(String[] args) {
        ConsoleClient consoleClient = new ConsoleClient();
        consoleClient.commandLoop();
    }
}