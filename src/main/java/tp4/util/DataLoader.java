package tp4.util;

import tp4.exception.DimensionNotFoundException;
import tp4.exception.NoDataFoundException;
import tp4.model.InstanceTSP;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class DataLoader {

    public InstanceTSP buildInstance(String fileName) {
        List<String> lines = null;
        try {
            lines = buildLines(fileName);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        InstanceBuilder instanceBuilder = new InstanceBuilder(fileName, lines);
        instanceBuilder.buildLineMap();
        return instanceBuilder.instanceTSP;
    }

    private List<String> buildLines(String fileName) throws FileNotFoundException {

        File file = new File(fileName);
        try (
                BufferedReader fileBuffer = new BufferedReader(new FileReader(file.getAbsolutePath()))
        ) {
            List<String> lines = new ArrayList<>();
            String line;

            while ((line = fileBuffer.readLine()) != null) {
                if (!line.trim().isEmpty()) {
                    lines.add(line);
                }
            }

            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new FileNotFoundException(fileName);
    }

    private enum LineType {
        COMMENT("COMMENT:"),
        DIMMENSION("DIMENSION:"),
        DATA("EDGE_WEIGHT_SECTION"),
        OTHER("OTHER");

        private String name;

        LineType(String name) {
            this.name = name;
        }

        static LineType of(String line) {
            return Arrays.stream(LineType.values())
                         .filter(type -> line.contains(type.name))
                         .findFirst().orElse(OTHER);
        }
    }

    private class InstanceBuilder {
        private List<String> lines;
        private String fileName;
        private InstanceTSP instanceTSP = new InstanceTSP();

        public InstanceBuilder(String fileName, List<String> lines) {
            this.lines = lines;
            this.fileName = fileName;
            List<String> prettyfileName = Arrays.asList(fileName.split("/"));
            instanceTSP.setFilename(prettyfileName.get(prettyfileName.size() - 1));
        }

        private String cleanLine(LineType type, String line) {
            return line.replace(type.name, "");
        }

        private void buildLineMap() {

            instanceTSP.setComment(lines.stream()
                                        .filter(line -> line.contains(LineType.COMMENT.name))
                                        .map(line -> cleanLine(LineType.of(line), line).trim())
                                        .findFirst().orElse(""));

            instanceTSP.setDimension(lines.stream()
                                          .filter(line -> line.contains(LineType.DIMMENSION.name))
                                          .map(line -> cleanLine(LineType.of(line), line).trim())
                                          .map(Integer::valueOf)
                                          .findFirst().orElseThrow(() -> new DimensionNotFoundException(fileName)));

            Integer firstDataLine = lines.stream()
                                         .filter(line -> line.contains(LineType.DATA.name))
                                         .map(line -> lines.indexOf(line) + 1)
                                         .findFirst()
                                         .orElseThrow(() -> new NoDataFoundException(fileName));



            String curratedFirstLine = lines.get(firstDataLine).replaceAll("^\\s+", "");
            curratedFirstLine = curratedFirstLine.replaceAll("\\s+$", "");
            curratedFirstLine = curratedFirstLine.replaceAll("[^\\d+\\s.]", "");
            List<String> firstline =  Arrays.asList(curratedFirstLine.split("\\s+"));
            Integer actualLineSize = firstline.size();
            Integer fileLineToDataLineRatio  = instanceTSP.getDimension() / actualLineSize;
            Integer lastDataLine = firstDataLine + fileLineToDataLineRatio * instanceTSP.getDimension();

            List<Integer> dataLines = lines.subList(firstDataLine, lastDataLine).stream()
                                           .flatMap(line -> Arrays.stream(line.split("\\s+"))
                                                                  .map(s -> s.replaceAll("[^\\d.]", ""))
                                                                  .filter(index -> !index.isEmpty())
                                                                  .filter(string -> !string.contains("."))
                                                                  .map(string -> Integer.parseInt(string.trim())))
                                           .collect(Collectors.toList());

            dataLinesToInstanceData(dataLines);

        }

        private void dataLinesToInstanceData(List<Integer> dataLines) {

            int dimension = instanceTSP.getDimension();
            int lineIndex = 0;

            Map<Integer, Map<Integer, Integer>> data = new HashMap<>();
            while (lineIndex < dimension) {
                Map<Integer, Integer> mapLine = new HashMap<>();
                for (int i = 0; i < dimension; i++) {
                    mapLine.put(i, dataLines.get((lineIndex * dimension) + i));
                }
                data.put(lineIndex, mapLine);
                lineIndex++;
            }

            instanceTSP.setData(data);
        }
    }

}




