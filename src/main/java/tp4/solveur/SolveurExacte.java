package tp4.solveur;

import tp4.exception.DataFormatException;
import tp4.model.Arc;
import tp4.model.InstanceTSP;
import tp4.model.Node;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class SolveurExacte extends AbstractSolveur {

    private Integer lowerBound;
    private Node allArcs;

    public SolveurExacte(InstanceTSP instance) {
        super(instance);
        lowerBound = 0;
    }

    public void solve() {
        mapAllToArc();
        lowerBound = getMinimalDistancesSum();
        simplifyData();
        lowerBound += getMinimalDistancesSum();
        calulateBestPath();
    }

    private void calulateBestPath() {
        List<Node> nodes = new ArrayList<>();

        //initialisation
        allArcs.getArcs().forEach(arc -> {
            Node nodeWithArc = new Node();
            nodeWithArc.add(arc);
            banAllArcToCity(arc.getFrom(), nodeWithArc);
            nodeWithArc.ban(allArcs.getReverse(arc));
            calculateAndSetNodeLowerBound(nodeWithArc, arc);

            nodes.add(nodeWithArc);
        });

        AtomicBoolean asASolution = new AtomicBoolean(false);
        while (!asASolution.get()) {

            for (int i = 0; i < nodes.size(); i++) {
                Node node = nodes.get(i);
                List<Arc> currentNodeArcs = node.getArcs();
                int lastArcInPathIndex = currentNodeArcs.size() - 1;

                boolean pathIsComplete = node.getArcs().size() == instance.getDimension();
                boolean solutionIsCorrect = node.getDistance() == lowerBound;
                if (pathIsComplete) {
                    if (solutionIsCorrect) {
                        asASolution.set(true);
                        System.out.println("\033[32m" + node.getArcs() + "\033[0m");
                    }
                }

                Optional<Arc> optionalNextArc = allArcs.getNext(currentNodeArcs.get(lastArcInPathIndex), node.getBannedArc());

                if (optionalNextArc.isPresent()) {
                    Arc nextArc = optionalNextArc.get();

                    boolean arcIsInNode = currentNodeArcs.contains(nextArc);
                    boolean arcBanned = node.getBannedArc().contains(nextArc);

                    if (!arcIsInNode && !arcBanned) {
                        Node sonWithArc = new Node(currentNodeArcs, node.getBannedArc(), node.getDistance(), node.getLowerBound());
                        Node sonWithoutArc = new Node(currentNodeArcs, node.getBannedArc(), node.getDistance(), node.getLowerBound());


                        int sonArcLastIndex = sonWithArc.getArcs().size();
                        if (sonArcLastIndex == instance.getDimension() - 1) {
                            banAllArcToCity(nextArc.getFrom(), sonWithArc);
                            int to = sonWithArc.getArcs().get(0).getFrom();
                            int from = sonWithArc.getArcs().get(sonArcLastIndex -1).getTo();
                            sonWithArc.add(allArcs.getArc(from, to));
                        } else {
                            if (nextArc.getFrom() == 3 && nextArc.getTo() == 1) {
                                System.out.println("THIS IS IT");
                            }
                            sonWithArc.add(nextArc);
                        }
                        banAllArcToCity(lastArcInPathIndex, sonWithArc);
                        Arc reverse = allArcs.getReverse(nextArc);
                        sonWithArc.ban(reverse);
                        sonWithoutArc.ban(nextArc);
                        calculateAndSetNodeLowerBound(sonWithArc, nextArc);
                        calculateAndSetNodeLowerBound(sonWithoutArc, nextArc);

                        nodes.add(sonWithArc);
                        nodes.add(sonWithoutArc);
                    }
                }
            }
            nodes.stream()
                 .filter(node -> node.getLowerBound() > lowerBound)
                 .map(nodes::indexOf)
                 .collect(Collectors.toList())
                 .forEach(nodes::remove);
        }
    }

    private void banAllArcToCity(Integer city, Node node) {
        allArcs.getArcs().stream()
               .filter(arc -> arc.getTo() == city)
               .forEach(node::ban);
    }


    private Node getBestSolution(List<Node> nodes) {
        Comparator<Node> compare = new Comparator<Node>() {
            @Override
            public int compare(Node n1, Node n2) {
                return n1.getDistance() - n2.getDistance();
            }
        };

        return nodes.stream()
                    .min(compare)
                    .orElseThrow(() -> new DataFormatException(instance.getFilename()));
    }

    private void calculateAndSetNodeLowerBound(Node node, Arc arc) {
        AtomicInteger nodeLowerBound = new AtomicInteger(lowerBound);

        nodeLowerBound.addAndGet(instance.getData()
                                         .get(arc.getFrom())
                                         .get(arc.getTo()));

        nodeLowerBound.addAndGet(-instance.getData().get(arc.getFrom()).values().stream()
                                          .min(Comparator.naturalOrder())
                                          .orElseThrow(() -> new DataFormatException(instance.getFilename())));

        node.setLowerBound(nodeLowerBound.get());
    }

    private int getMinimalDistancesSum() throws DataFormatException {
        return instance.getData().values().stream()
                       .map(line -> line.values().stream()
                                        .min(Comparator.naturalOrder())
                                        .orElseThrow(() -> new DataFormatException(instance.getFilename())))
                       .mapToInt(i -> i)
                       .sum();
    }

    public void simplifyData() {
        Map<Integer, Map<Integer, Integer>> curratedInstance = new HashMap<>(instance.getData());
        curratedInstance.entrySet().forEach(this::substractMinimalDistanceVertical);
        substractMinimalDistanceHorizontal(curratedInstance);
    }

    private void substractMinimalDistanceVertical(Map.Entry<Integer, Map<Integer, Integer>> entry) {
        Integer min = getLineMin(entry);

        entry.getValue().entrySet()
             .forEach(line -> line.setValue(line.getValue() - min));
    }


    private void substractMinimalDistanceHorizontal(Map<Integer, Map<Integer, Integer>> data) {
        data.forEach((city, values) -> {
            Integer min = getColumnMin(data, city);
            if (min != 0) {
                updateColumnMin(data, city, min);
                lowerBound += min;
            }
        });

    }

    private void updateColumnMin(Map<Integer, Map<Integer, Integer>> data, Integer index, Integer min) {
        data.forEach((key, value) -> value.entrySet().stream()
                                          .filter(entry -> entry.getKey().equals(index))
                                          .forEach(entry -> entry.setValue(entry.getValue() - min)));
    }

    private Integer getColumnMin(Map<Integer, Map<Integer, Integer>> data, Integer index) {
        return data.entrySet().stream()
                   .flatMap(integerMapEntry -> integerMapEntry.getValue().entrySet().stream()
                                                              .filter(entry -> entry.getKey().equals(index)))
                   .map(Map.Entry::getValue)
                   .min(Comparator.naturalOrder())
                   .orElseThrow(() -> new DataFormatException(instance.getFilename()));
    }

    private Integer getLineMin(Map.Entry<Integer, Map<Integer, Integer>> entry) {
        return entry.getValue().values().stream()
                    .min(Comparator.naturalOrder())
                    .orElseThrow(() -> new DataFormatException(instance.getFilename()));
    }

    private void mapAllToArc() {
        allArcs = new Node();
        instance.getData().forEach((from, integerIntegerMap) -> {
            integerIntegerMap.entrySet().stream()
                             .filter(entry -> !entry.getKey().equals(from))
                             .forEach((city) -> allArcs.add(from, city.getKey(), city.getValue()));
        });

        allArcs.getArcs().forEach(System.out::println);
    }

    private Arc mapToArc(int i, int j, int distance) {
        return new Arc(i, j, distance);
    }
}
