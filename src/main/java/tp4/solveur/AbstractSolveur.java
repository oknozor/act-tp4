package tp4.solveur;

import tp4.model.InstanceTSP;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AbstractSolveur {
    protected InstanceTSP instance;
    protected List<Boolean> visitedCities;
    protected List<Integer> path;
    protected Integer startCity;
    protected Integer distance;

    public AbstractSolveur(InstanceTSP instance) {
        this.instance = instance;
        visitedCities = instance.getData().entrySet().stream()
                                .map(inst -> Boolean.FALSE)
                                .collect(Collectors.toList());
        path = new ArrayList<>();
        this.distance = 0;
    }

    protected void countDistanceFromCityList(List<Integer> cityList) {
        int size = cityList.size() - 1;
        IntStream.range(0, size).forEach(currentIndex -> {
            int currentCity = cityList.get(currentIndex);
            int nextCity = cityList.get(currentIndex + 1);
            distance += instance.getData().get(currentCity).get(nextCity);
        });
    }

    public InstanceTSP getInstance() {
        return instance;
    }

    public List<Integer> getPath() {
        return path;
    }

    public void setStartCity(Integer startCity) {
        this.startCity = startCity;
    }

    public Integer getDistance() {
        return distance;
    }
}
