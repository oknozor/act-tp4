package tp4.solveur;

import tp4.model.InstanceTSP;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SolveurAjoutPlusProche  extends AbstractSolveur {

    public SolveurAjoutPlusProche(InstanceTSP instance) {
        super(instance);
    }

    private void moveFromCurrentToNextCity(Integer currentCity) {
        Map<Integer, Integer> currentCityData = instance.getData().get(currentCity);
        Integer closestCity = this.getClosestMinimum(currentCityData);
        path.add(closestCity);
    }

    private void initStartCity(Integer startCity) {
        this.startCity = startCity;
        path.add(startCity);
        visitedCities.set(startCity, true);
    }

    private Boolean isNotVisited(Map.Entry<Integer, Integer> city) {
        return !visitedCities.get(city.getKey());
    }

    public Integer getClosestMinimum(Map<Integer, Integer> line) {

        return line.entrySet().stream()
                   .filter(v -> v.getValue() != 0)
                   .filter(this::isNotVisited)
                   .min(Comparator.comparing(Map.Entry::getValue))
                   .map(Map.Entry::getKey)
                   .orElse(startCity);
    }

    public List<Integer> buildFullPath() {
        initStartCity(startCity);
        while (visitedCities.contains(Boolean.FALSE)) {
            int lastVisited = path.get(path.size() - 1);
            visitedCities.set(lastVisited, true);
            moveFromCurrentToNextCity(lastVisited);
        }
        countDistanceFromCityList(path);
        return path;
    }
}
