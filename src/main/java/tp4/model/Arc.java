package tp4.model;

import java.util.Objects;

public class Arc {
    private int from;
    private int to;
    private int distance;

    public Arc(int from, int to, int distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Arc)) return false;
        Arc arc = (Arc) o;
        return Objects.equals(from, arc.from) &&
                Objects.equals(to, arc.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    @Override
    public String toString() {
        return "[" +this.from + ";" + this.to + ";" + this.distance + "]";
    }

}
