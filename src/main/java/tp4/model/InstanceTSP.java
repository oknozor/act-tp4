package tp4.model;

import java.util.Map;
import java.util.stream.IntStream;

public class InstanceTSP {
    private String filename;
    private String comment;
    private int dimension;
    private int distance;
    private Map<Integer, Map<Integer, Integer>> data;

    public InstanceTSP() {
        this.comment = null;
        this.dimension = 0;
        this.data = null;
    }
    public InstanceTSP(String comment, int dimension, Map<Integer, Map<Integer, Integer>> data, String filename) {
        this.comment = comment;
        this.dimension = dimension;
        this.data = data;
        this.filename = filename;
    }

    private void countDistance() {
        int size = this.dimension - 1;
        IntStream.range(0, size).forEach(currentIndex -> {
            distance += this.getData().get(currentIndex).get(currentIndex +1);
        });
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public Map<Integer, Map<Integer, Integer>> getData() {
        return data;
    }

    public void setData(Map<Integer, Map<Integer, Integer>> data) {
        this.data = data;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getDistance() {
        countDistance();
        return distance;
    }
}
