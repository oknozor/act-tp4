package tp4.model;

import tp4.exception.DataFormatException;

import java.util.*;

public class Node {
    private List<Arc> arcs;
    private List<Arc> bannedArc;
    private int distance;
    private int lowerBound;

    public Node() {
        this.arcs = new ArrayList<>();
        this.bannedArc = new ArrayList<>();
        this.distance = 0;
    }

    public Node(List<Arc> arcs, List<Arc> bannedArc, int distance, int lowerBound) {
        this.arcs = arcs;
        this.bannedArc = bannedArc;
        this.distance = distance;
        this.lowerBound = lowerBound;
    }

    public void add(Arc arc) {
        arcs.add(arc);
        distance += arc.getDistance();
    }

    public void add(Integer from, Integer to, Integer distance) {
        arcs.add(new Arc(from, to, distance));
    }

    public List<Arc> getArcs() {
        return arcs;
    }

    public void ban(Arc arc) {
        bannedArc.add(arc);
    }

    public List<Arc> getBannedArc() {
        return bannedArc;
    }

    public Arc getReverse(Arc arc) {
        return
                arcs.stream()
                    .filter(a -> a.getFrom() == arc.getTo())
                    .filter(a -> a.getTo() == arc.getFrom())
                    .findFirst().orElseThrow(() -> new DataFormatException(""));
    }

    public Optional<Arc> getNext(Arc arc, List<Arc> arcs) {
        return this.arcs.stream()
                        .filter(next -> !arcs.contains(next))
                        .filter(next -> next.getFrom() == arc.getTo())
                        .filter(next -> !bannedArc.contains(next))
                        .findFirst();
    }

    public Arc getArc(int from, int to) {
        return arcs.stream()
                   .filter(arc -> arc.getFrom() == from)
                   .filter(arc -> arc.getTo() == to)
                   .findFirst()
                .orElseThrow(() -> new DataFormatException(""));
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return distance == node.distance &&
                lowerBound == node.lowerBound &&
                Objects.equals(arcs, node.arcs) &&
                Objects.equals(bannedArc, node.bannedArc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(arcs, bannedArc, distance, lowerBound);
    }
}
