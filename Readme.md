#TP4 : Que faire face à un problème dur?

##1 Heuristiques globales
La méthode choisie est la construction itérative par ajout du plus proche. On remarque qu'on ne trouve pas toujours la solution optimale.

##2 Méthodes locales
L'implémentation choisie et HillClimbing. Cette méthode permet d'affiner les résultats obtenus à l'aide de la première méthode, mais ne permet toujours pas d'obtenir le bon résultat à chaque fois.

